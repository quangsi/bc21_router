import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";

export default class HeaderComponent extends Component {
  render() {
    return (
      <div>
        <button className="btn btn-success  mx-2">
          <NavLink
            className=" text-white"
            to="/"
            exact
            activeClassName="p-2  text-danger"
          >
            Home page
          </NavLink>
        </button>

        <button className="btn btn-warning mx-2 ">
          <NavLink
            className=" text-white"
            to="/detail"
            activeClassName="p-2 text-danger"
          >
            Detail
          </NavLink>
        </button>
        <button className="btn btn-secondary mx-2 ">
          <NavLink
            className=" text-white"
            to="/login"
            activeClassName="p-2 text-danger"
          >
            Login
          </NavLink>
        </button>
      </div>
    );
  }
}
