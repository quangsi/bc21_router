import axios from "axios";
import React, { Component } from "react";

export default class DetailPage extends Component {
  componentDidMount() {
    console.log(this.props);

    let { id } = this.props.match.params;
    console.log("id", id);

    axios
      .get(`https://62231068666291106a33d218.mockapi.io/mon-an/${id}`)
      .then((res) => {
        console.log("chi tiết", res);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    return <div>DetailPage</div>;
  }
}
