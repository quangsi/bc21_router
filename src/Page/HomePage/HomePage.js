import axios from "axios";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class HomePage extends Component {
  state = {
    sanhSachMonAn: [],
  };
  componentDidMount() {
    axios
      .get("https://62231068666291106a33d218.mockapi.io/mon-an")
      .then((res) => {
        console.log(res);
        this.setState({ sanhSachMonAn: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    return (
      <div className="container py-5">
        <div className="row">
          {this.state.sanhSachMonAn.map((item) => {
            return (
              <div className="card text-left col-3">
                <div className="card-body">
                  <h4 className="card-title">{item.name}</h4>
                  <p className="card-text">{item.price}</p>
                  <button>
                    <NavLink
                      to={`/detail/${item.id}`}
                      className="btn btn-warning d-inline"
                    >
                      Xem chi tiết
                    </NavLink>
                  </button>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
