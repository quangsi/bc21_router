import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import { Route } from "react-router-dom";
import HomePage from "./Page/HomePage/HomePage";
import DetailPage from "./Page/DetailPage/DetailPage";
import { Switch } from "react-router-dom";
import HeaderComponent from "./components/Header/Header.component";
import LoginPage from "./Page/LoginPage/LoginPage";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <HeaderComponent />
        <Switch>
          {/* <Route path="/" exact component={HomePage} /> */}

          <Route
            path="/"
            exact
            render={() => {
              return <HomePage />;
            }}
          />
          <Route path="/detail/:id" component={DetailPage} />
          <Route path="/login" component={LoginPage} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
